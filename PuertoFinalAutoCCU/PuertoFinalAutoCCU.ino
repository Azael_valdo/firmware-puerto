
/*Autor 
 * 
 * Azael Ortega Valdovinos
 * 
*/

#include <SPI.h>
#include <Ethernet.h>
#include <MFRC522.h>
#include <EEPROM.h>
#define PING_TIMEOUT 5000 
//Consultar relacion para determinar que parametros corresponden.

byte ip[] = {192, 168, 0, 38};
byte mac[] = {0x3a,0x1a,0xf3,0x19,0x1d,0xc5};
String uID = "EVT_UID#362500651f#";


byte server[] = { 192, 168, 0, 2}; //No cambiar

#define RST_PIN         8 
#define SS_PIN          9

// Crea la instancia para la lectora de tags RFID-MFRC522
MFRC522 mfrc522(SS_PIN, RST_PIN);   
MFRC522::MIFARE_Key key;

EthernetClient client;

char bandera; //bandera de estado de leds
const long intervalo = 500;  //intervalo de parpadeo
unsigned long T1 = 0;  //tiempo actual
unsigned long T2 = 0;  //tiempo anterior
int valLed = 0; //valor para parpadeo
int valLed2 = 0; //para el segundo color
int sensValue = -1; //Bandera para el estado del puerto
char c; //Variable que controla el case principal del loop
unsigned long lastPing = 0; //Bandera que controla el estado de la conexion del puerto con el server
int d = 0; //bandera que manda a cerrar el candado en caso de que se haya perdido la conexion con el servidor

void setup() {
  Serial.begin(9600);
  Ethernet.begin(mac, ip);

  // Se inicia comunicacion con  mfrc522 a traves de protocolo SPI.
  SPI.begin();
  mfrc522.PCD_Init();

  pinMode(5, OUTPUT);
  pinMode(3, OUTPUT);
  pinMode(6, OUTPUT);
  pinMode(4, OUTPUT);
  pinMode(2, OUTPUT);
  pinMode(7, INPUT);
  delay(1000);
}


/*
Rutina principal 
*/
void loop() {
  int sensorValue0 = digitalRead(7);
  if (!client.connected() || lastPing + PING_TIMEOUT < millis() ) {
    if (d == 1) operarCandado(2); d = 0;
    Serial.println("Se perdio la conexion con el servidor, reconectando...");
    client.stop();
    bandera = '6';
    conectar();
  }
  if (client.available()) {
    c = client.read();
    d = 1;
    switch (c) {

      case '1' :
        operarLed(1);

        if (client.connected())
          client.print("1");
        lastPing = millis();
        bandera = '0';
        break;

      case '2':
        operarLed(2);

        if (client.connected())
          client.print("1");
        lastPing = millis();
        bandera = '0';
        break;

      case '3':
        operarLed(3);

        if (client.connected())
          client.print("1");
        lastPing = millis();
        bandera = '0';
        break;

      case '4':

        if (client.connected())
          client.print("1");
        lastPing = millis();
        bandera = '4';
        break;

      case '5':

        if (client.connected())
          client.print("1");
        lastPing = millis();
        bandera = '5';
        break;

      case '6':

        if (client.connected())
          client.print("1");
        lastPing = millis();
        bandera = '6';
        break;

      case '7':
        operarLed(4);

        if (client.connected())
          client.print("1");
        lastPing = millis();
        bandera = '0';
        break;

      case '8':
        operarCandado(1);

        if (client.connected())
          client.print("1");
        lastPing = millis();
        break;

      case '9':
        operarCandado(2);

        if (client.connected())
          client.print("1");
        lastPing = millis();
        break;
      case'p' :

        if (client.connected())
          client.print("1");
        lastPing = millis();
        break;
      case 'e' :
        if (sensorValue0 == HIGH) {
          if (client.connected())
            client.print("0");
          delay(300);
        }
        else if (client.connected())
          client.print("1");
        delay(300);
        lastPing = millis();
        break;
      default:
        break;
    }
  }
  start_reading();
  parpadeoLed(); 
  portStatus(sensorValue0);
}


/*
 * Funcion para operar el color que encendera el led
  Modo 1 : Enciende led color verde
  Modo 2 : Enciende led color naranja
  Modo 3 : Enviende led color rojo
  Modo 4 : Apaga led
*/

void operarLed(int modo) {
  switch (modo) {
    case 1 :
      analogWrite(3, 0);
      analogWrite(5, 255);
      analogWrite(6, 0);
      break;
    case 2 :
      analogWrite(3, 255);
      analogWrite(5, 30);
      analogWrite(6, 0);
      break;
    case 3 :
      analogWrite(3, 255);
      analogWrite(5, 0);
      analogWrite(6, 0);
      break;
    case 4:
      analogWrite(3, 0);
      analogWrite(5, 0);
      analogWrite(6, 0);
      break;
  }
}

/*
 * Funcion que opera el funcionamiento del cadado 
Modo 1 : Abre candado
Modo 2 : Cierra candado
*/

void operarCandado(int modo) {
  switch (modo) {
    case 1 :
      digitalWrite(4, HIGH);
      digitalWrite(2, LOW);
      delay(500);
      digitalWrite(4, LOW);
      break;
    case 2 :
      digitalWrite(4, LOW);
      digitalWrite(2, HIGH);
      delay(500);
      digitalWrite(2, LOW);
      break;
  }
}

/*
Metodo que realiza la conexion TCP/IP e imprime el id del dispositivo
*/
void conectar() {
  Serial.println("Conectando...");
  if (client.connect(server, 8194)) {
    client.println(uID);
    bandera = '5';
    delay(1000);
    //client.println("Apagado");
    Serial.println("Conectado");
  } else {
    Serial.println("Fallo conexion");
    bandera = '6';

  }
}
/*
 * Metodo que devuelve el estado(ocupado/libre) actual del puerto.
*/
void portStatus(int sensorValue0) {
  int _sensValue;
  if (sensorValue0 == HIGH) _sensValue = 0;
  else _sensValue = 1;
  if (sensValue != _sensValue) {
    sensValue = _sensValue;
    switch (sensValue) {
      case 1:
        client.println("EVT_OCUPADO");
        break;
      case 0:
        client.println("EVT_VACIO");
        break;
    }
  }
}


/*
 * Metodo ocupado para realizar la lectura del tag RFID pegado a la bicicleta
*/
void start_reading() {
  if ( ! mfrc522.PICC_IsNewCardPresent())
    return;
  if ( ! mfrc522.PICC_ReadCardSerial())
    return;
  dump_byte_array(mfrc522.uid.uidByte, mfrc522.uid.size);
  Serial.println();
  mfrc522.PICC_HaltA();
  mfrc522.PCD_StopCrypto1();
}


/*
 * Metodo ocupado para realizar la conversion del arreglo de bytes obtenido del tag RFID a un string
*/
void dump_byte_array(byte *buffer, byte bufferSize) {
  String inputString = "EVT_RFID#";
  for (byte i = 0; i < bufferSize; i++) {  //Se acumula byte por byte en un arreglo y se convierte a string.
    inputString += buffer[i];
  }
  inputString += '#';
  client.println(inputString);
  inputString = "";
}

/*
 * Metodo que se encarga de operar el parpadeo del led, es controlado por una bandera global que se actualiza en los cases 4,5,6 del siwtch del loop
*/
void parpadeoLed() 
{ 
  T1 = millis();
  switch (bandera)
  {
    case '4':
      if (T1 - T2 >= intervalo)
      {
        if (valLed == 0) {
          valLed = 255;
        }
        else {
          valLed = 0;
        }
        T2 = T1;
        analogWrite(3, 0);
        analogWrite(5, valLed);
        analogWrite(6, 0);
      }
      break;
    case '5':
      if (T1 - T2 >= intervalo)
      {
        if (valLed == 0) {
          valLed = 255;
          valLed2 = 30;
        }
        else {
          valLed = 0;
          valLed2 = 0;
        }
        T2 = T1;
        analogWrite(3, valLed);
        analogWrite(5, valLed2);
        analogWrite(6, 0);
      }
      break;
    case '6':
      if (T1 - T2 >= intervalo)
      {
        if (valLed == 0) {
          valLed = 255;
        }
        else {
          valLed = 0;
        }
        T2 = T1;
        analogWrite(3, valLed);
        analogWrite(5, 0);
        analogWrite(6, 0);
      }
      break;
      
  }

}

