#include <SPI.h>
#include <Ethernet.h>

byte mac[] = {0x90, 0xA2,0xDA,0x0D,0x6B,0xC4};
byte ip []={192,168,0,113};
byte gateway[]={192,168,0,1};
byte subnet[] = {255,255,255,0};


EthernetServer server(23);

boolean hayMensaje = false;
boolean estadoLed1 = false, estadoLed2= false, estadoLed3 = false;

void setup(){

  pinMode(5, OUTPUT);
  pinMode(3, OUTPUT);
  pinMode(9, OUTPUT);
  pinMode(7, OUTPUT);
  pinMode(8, OUTPUT);
Ethernet.begin(mac);

  server.begin();
}

void loop(){
  EthernetClient cliente = server.available();

  if (cliente){
    if(hayMensaje){
      cliente.println("Hola");
      hayMensaje = true;
    }

    char data = cliente.read();

    switch(data){
      case '1' : 
          analogWrite(3, 0);
          analogWrite(5, 255);
          analogWrite(9, 0);
          cliente.println("verde");
           break;
       case '2':
           analogWrite(3, 255);
          analogWrite(5, 30);
          analogWrite(9, 0);
           cliente.println("naranja");
           break;
       case '3':
           analogWrite(3, 255);
          analogWrite(5, 0);
          analogWrite(9, 0);
           cliente.println("rojo");
           break;
       case '4':
           //digitalWrite(13,estadoLed3);
           cliente.println("led3");
           
           break;
       case '5':
           //digitalWrite(13,estadoLed3);
           cliente.print("led3");
           
           break;
       case '6':
           //digitalWrite(13,estadoLed3);
           cliente.print("led3");
          
           break;
       case '7':
           analogWrite(3, 0);
          analogWrite(5, 0);
          analogWrite(9, 0);
           cliente.println("Apagado");
           break;
       case '8':
           digitalWrite(8,HIGH);
           digitalWrite(7,LOW);
           delay(1000);
           digitalWrite(8,LOW);
           cliente.println("abierto");
           
           break;
       case '9':
           digitalWrite(8,LOW);
           digitalWrite(7,HIGH);
           delay(1000);
           digitalWrite(7,LOW);
           cliente.println("cerrado");
          
           break;
       default: 
           break;
      }
  }
  }
